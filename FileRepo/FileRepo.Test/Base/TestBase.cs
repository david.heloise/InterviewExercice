﻿using System;
using System.IO;
using NUnit.Framework;
using FileRepo.Data;
using FileRepo.Data.Repository;

namespace FileRepo.Test.Base
{
    public class TestBase
    {
        protected FileRepoDbContext FileRepoDbContext;

        [SetUp]
        public virtual void TestSetUp()
        {
            FileRepoDbContext = new FileRepoDbContext();
            var dr = new DocumentRepository(FileRepoDbContext);
            var pr = new ProjectRepository(FileRepoDbContext);
            foreach (var document in dr.GetAll().Result)
                dr.Remove(document);
            foreach (var project in pr.GetAll().Result)
                pr.Remove(project);

            FileRepoDbContext.SaveChanges();
        }


        [TearDown]
        public virtual void TestTearDown()
        {
            FileRepoDbContext.Dispose();
        }

    }
}
