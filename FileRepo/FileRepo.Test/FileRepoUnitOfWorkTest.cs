﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FileRepo.Data.Repository;
using FileRepo.Test.Base;

namespace FileRepo.Test.RepositoriesTests
{
    [TestFixture]
    class FileRepoUnitOfWorkTest :TestBase
    {
        [Test]
        public void TestUsesTheSameContext()
        {
            using (var uow = new FileRepoUnitOfWork(FileRepoDbContext))
            {
                var projectRepo = uow.Projects as ProjectRepository;
                var documentRepo = uow.Documents as DocumentRepository;

                Assert.IsNotNull(projectRepo,"ProjectRepository should be null");
                Assert.IsNotNull(documentRepo, "DocumentRepository should be null");

                var pCtx = projectRepo.FileRepoDbContext;
                var dCtx = documentRepo.FileRepoDbContext;

                Assert.IsNotNull(pCtx, "Context should be null");
                Assert.IsNotNull(dCtx, "Context should be null");

                Assert.AreSame(pCtx,dCtx);
            }
        }

        [Test]
        public void TestComitSaveData()
        {
            using (var uow = new FileRepoUnitOfWork(FileRepoDbContext))
            {
                uow.Documents.Add(TestHelper.GetOneDocument());
                var d = uow.Documents.GetAll().Result.FirstOrDefault();
                Assert.IsNull(d);
                uow.Commit();
                d = uow.Documents.GetAll().Result.FirstOrDefault();
                Assert.IsNotNull(d);

                var projectToAdd = TestHelper.GetOneEmptyOneProject();
                uow.Projects.Add(projectToAdd);
                var p = uow.Projects.GetAll().Result.Single();
                Assert.AreEqual(d.Parent.Name, p.Name);
                uow.Commit();

                p = uow.Projects.Find(project => project.Name == d.Parent.Name).Result.Single();
                var p2 = uow.Projects.Find(project => project.Name == projectToAdd.Name).Result.Single();

            }
        }

    }
}
