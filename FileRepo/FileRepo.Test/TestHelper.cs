﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Model;

namespace FileRepo.Test
{
    public class TestHelper
    {
        private static int pCount;
        private static int dCount;

        public static Project GetOneEmptyOneProject()
        {
            return new Project { Name = $"P{pCount++}" };
        }
        //public static IEnumerable<Project> GetNEmptyProject(int n)
        //{
        //    for (int i = 0; i < n; i++)
        //        yield return new Project {Name = $"P{pCount++}"};
        //}

        public static Document GetOneDocument()
        {
            return GetNDocument(1).First();
        }

        public static IEnumerable<Document> GetNDocument(int n, bool withParent=true)
        {
            var parent = withParent ? GetOneEmptyOneProject() : null;
            for (int i = 0; i < n; i++)
                yield return new Document { Name = $"D{dCount++}", Value = new byte[0] ,Parent = parent, Extension = "xls"};
        }

        public static RepositoryEntitytBase GetElementBase(Type type)
        {

            if (type == typeof (Document))
                return GetOneDocument();

            if (type == typeof (Project))
                return GetOneEmptyOneProject();

            if(type==typeof(RepositoryEntitytBase))
                return new RepositoryEntitytBase();

            return null;
        }

        public static void ClearAllData()
        {
            
        }
    }
}
