﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FileRepo.Data.Repository;
using FileRepo.Data.Repository.Base;
using FileRepo.Model;
using FileRepo.Test.Base;

namespace FileRepo.Test.RepositoriesTests
{
    [TestFixture(typeof (Document), typeof (DocumentRepository))]
    [TestFixture(typeof (Project), typeof (ProjectRepository))]
    [TestFixture(typeof (Document), typeof (Repository<Document>))]
    [TestFixture(typeof (Project), typeof (Repository<Project>))]
    public class RepositoryTest<TEntity, TRepo> : TestBase where TEntity : RepositoryEntitytBase
        where TRepo : IRepository<TEntity>
    {
        private TEntity _entity;
        private TRepo _repo;
        private const string ENTITY_NOTFOUND = "Entity not found";
        private const string ENTITY_NOTSAME = "Entity should be the same";

        private void BuildRepoAndEntity()
        {
            _repo = (TRepo) Activator.CreateInstance(typeof (TRepo), FileRepoDbContext as DbContext);
            _entity = TestHelper.GetElementBase(typeof (TEntity)) as TEntity;
        }

        [SetUp]
        public override void TestSetUp()
        {
            base.TestSetUp();
            BuildRepoAndEntity();
        }

        private void InstanceCheck()
        {
            Assert.IsNotNull(_repo, "Repository should not be null");
            Assert.IsNotNull(_entity, "Entity should not be null");
            Assert.AreEqual(0, _repo.GetAll().Result.Count(), "Repository should be empty");
        }

        [Test]
        public void TestAdd()
        {
            InstanceCheck();

            var expectedEntityName = _entity.Name;

            _repo.Add(_entity);
            FileRepoDbContext.SaveChanges();

            var result = _repo.Add(null);
            Assert.IsFalse(result.HasSucceed,"Insert null into the repository should have fail");
            _entity.Name = "";

            _repo.Add(_entity);
            Assert.Throws(typeof (DbEntityValidationException), () => FileRepoDbContext.SaveChanges(),
                "Insert entity with no name should have fail");

            var entities = _repo.GetAll().Result.ToArray();
            Assert.AreEqual(1, entities.Count(), ENTITY_NOTFOUND);
            Assert.AreEqual(expectedEntityName, entities[0].Name, ENTITY_NOTSAME);
        }

        [Test]
        public void TestRemove()
        {
            // Since test should not rely on other test I have to copy/paste the insert
            InstanceCheck();

            _repo.Add(_entity);
            FileRepoDbContext.SaveChanges();
            var entities = _repo.GetAll().Result.ToArray();

            Assert.AreEqual(1, entities.Count(), ENTITY_NOTFOUND);
            Assert.AreEqual(_entity.Name, entities[0].Name, ENTITY_NOTSAME);

            _repo.Remove(_entity);
            FileRepoDbContext.SaveChanges();
            entities = _repo.GetAll().Result.ToArray();
            Assert.AreEqual(0, entities.Count(), "No entity should have been found");
        }

        [Test]
        public void TestGet()
        {
            // Since test should not rely on other test I have to copy/paste the insert
            InstanceCheck();

            _repo.Add(_entity);
            FileRepoDbContext.SaveChanges();
            var entity = _repo.Get(_entity.Id);

            Assert.IsNotNull(entity, ENTITY_NOTFOUND);
            Assert.AreEqual(_entity.Name, entity.Result.Name, ENTITY_NOTSAME);

        }

        [Test]
        public void TestGetAll()
        {
            InstanceCheck();
            var entities = new List<TEntity>();
            const int nbrEntitites = 10;
            for (var i = 0; i < nbrEntitites; i++)
            {
                var entity = TestHelper.GetElementBase(typeof (TEntity)) as TEntity;
                entities.Add(entity);

                Assert.IsNotNull(entity, "Unable to properly instanciate the entity");
                _repo.Add(entity);
            }
            FileRepoDbContext.SaveChanges();

            var retrievedEntites = _repo.GetAll().Result.ToArray();
            Assert.AreEqual(10, retrievedEntites.Count(), "Number of entities retrieved");

            foreach (var entity in entities)
            {
                try
                {
                    retrievedEntites.Single(e => e.Name == entity.Name);
                }
                catch (Exception)
                {
                    Assert.Fail($"Entity {entity.Name} not found.");
                }
            }
        }

        [Test]
        public void TestFind()
        {
            InstanceCheck();
            var entities = new List<TEntity>();
            const int nbrEntitites = 10;
            for (var i = 0; i < nbrEntitites; i++)
            {
                var entity = TestHelper.GetElementBase(typeof (TEntity)) as TEntity;
                entities.Add(entity);

                Assert.IsNotNull(entity, "Unable to properly instanciate the entity");
                _repo.Add(entity);
            }
            _repo.Add(_entity);
            FileRepoDbContext.SaveChanges();

            var retrievedEntites = _repo.Find(entity => entity.Name == _entity.Name);
            Assert.AreEqual(1, retrievedEntites.Result.Count(), "Number of entities retrieved");
        }

    }
}
