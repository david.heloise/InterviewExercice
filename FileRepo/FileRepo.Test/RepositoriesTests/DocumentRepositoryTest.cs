﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FileRepo.Data;
using FileRepo.Data.Repository;
using FileRepo.Model;
using FileRepo.Test.Base;

namespace FileRepo.Test.RepositoriesTests
{
    [TestFixture]
    public class DocumentRepositoryTest : TestBase
    {

        [Test]
        public void TestGetLatestDocument()
        {
            const string nameOfTheLatest = "TheOne";
            FileRepoDbContext.Documents.AddRange(TestHelper.GetNDocument(4));

            FileRepoDbContext.SaveChanges();
            var d = TestHelper.GetOneDocument();
            d.Name = nameOfTheLatest;
            FileRepoDbContext.Documents.Add(d); // to make sure that it is the lastest to be inserted
            FileRepoDbContext.SaveChanges();

            var drt = new DocumentRepository(FileRepoDbContext);
            var theLatestDocumentFound = drt.GetLastestDocument();
            Assert.IsNotNull(theLatestDocumentFound, "no record found in the database");
            Assert.AreEqual(nameOfTheLatest, theLatestDocumentFound.Result.Name, "The document found is not the expected one.");

        }

        [Test]
        public void TestFindByUrl()
        {
            #region Test data
            var p1 = TestHelper.GetOneEmptyOneProject();
            var p1A = TestHelper.GetOneEmptyOneProject();
            var p1AA = TestHelper.GetOneEmptyOneProject();

            var p2 = TestHelper.GetOneEmptyOneProject();
            var p1B = TestHelper.GetOneEmptyOneProject();
            var p1BB = TestHelper.GetOneEmptyOneProject();

            var d1 = TestHelper.GetOneDocument();
            var d1A = TestHelper.GetOneDocument();
            var d1AA = TestHelper.GetOneDocument();

            p1.AddSubProject(p1A);
            p1.AddSubProject(p1B);

            p1A.AddSubProject(p1AA);
            p1A.AddSubProject(p1BB);
            p1.AddDocument(d1);
            p1A.AddDocument(d1A);
            p1AA.AddDocument(d1AA);

            var d1Url = d1.Url;
            var d1AUrl = d1A.Url;
            var d1AAUrl = d1AA.Url; 
            #endregion

            FileRepoDbContext.Projects.AddRange(new List<Project>{p1, p2});
            FileRepoDbContext.SaveChanges();

            var dRepo = new DocumentRepository(FileRepoDbContext);

            d1 = dRepo.FindByUrl(d1Url).Result;
            d1A = dRepo.FindByUrl(d1AUrl).Result;
            d1AA = dRepo.FindByUrl(d1AAUrl).Result;

            const string errMsg = "Document not found using url";

            Assert.AreEqual(d1Url,d1.Url,errMsg);
            Assert.AreEqual(d1AUrl, d1A.Url, errMsg);
            Assert.AreEqual(d1AAUrl, d1AA.Url, errMsg);

        }

    }
    
}
