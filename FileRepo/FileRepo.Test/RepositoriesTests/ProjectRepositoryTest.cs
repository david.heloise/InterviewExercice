﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FileRepo.Data;
using FileRepo.Data.Repository;
using FileRepo.Model;
using FileRepo.Test.Base;

namespace FileRepo.Test.RepositoriesTests
{
    [TestFixture]
    public class ProjectRepositoryTest : TestBase
    {
        [Test]
        public void TestGetBiggestProject()
        {
            const string nameOfTheBiggest = "TheOne";

            #region Test data

            var p1 = TestHelper.GetOneEmptyOneProject();

            var p2 = TestHelper.GetOneEmptyOneProject();
            p2.Documents = TestHelper.GetNDocument(2,false).ToList();

            var p3 = TestHelper.GetOneEmptyOneProject();
            p3.Documents = TestHelper.GetNDocument(4, false).ToList(); // same quantity as p4

            var p4 = TestHelper.GetOneEmptyOneProject();
            p4.Documents = TestHelper.GetNDocument(4, false).ToList(); // same quantity as p3
            p4.Name = nameOfTheBiggest;

            #endregion

            FileRepoDbContext.Projects.AddRange(new List<Project> {p1, p2, p3});
            FileRepoDbContext.SaveChanges();
            FileRepoDbContext.Projects.Add(p4); // to make sure that it is the lastest to be inserted
            FileRepoDbContext.SaveChanges();

            var drt = new ProjectRepository(FileRepoDbContext);
            var theBiggestFound = drt.GetBiggestProject();
            Assert.IsNotNull(theBiggestFound, "no record found in the database");
            Assert.AreEqual(nameOfTheBiggest, theBiggestFound.Result.Name, "The document found is not the expected one.");
        }

        [Test]
        public void TestGetAllRootProjects()
        {
            #region Test data

            var names = new List<string>{ "P1", "P1a", "P1b", "P2", "P3", "P3a", "P3aa", "PP3b"};

            var p1 = TestHelper.GetOneEmptyOneProject();
            var p1a = TestHelper.GetOneEmptyOneProject();
            var p1b = TestHelper.GetOneEmptyOneProject();
            p1.Name = names[0];
            p1a.Name = names[1];
            p1b.Name = names[2];
            p1.AddSubProject(p1a);
            p1.AddSubProject(p1b);

            var p2 = TestHelper.GetOneEmptyOneProject();
            p2.Name = names[3];

            var p3 = TestHelper.GetOneEmptyOneProject();
            var p3a = TestHelper.GetOneEmptyOneProject();
            var p3aa = TestHelper.GetOneEmptyOneProject();
            var p3b = TestHelper.GetOneEmptyOneProject();
            p3.Name = names[4];
            p3a.Name = names[5];
            p3aa.Name = names[6];
            p3b.Name = names[7];

            p3.AddSubProject(p3a);
            p3a.AddSubProject(p3aa);
            p3.AddSubProject(p3b);
            
            #endregion

            using (var uow = new FileRepoUnitOfWork(new FileRepoDbContext()))
            {
                new List<Project> { p1, p2, p3 }.ForEach(project => uow.Projects.Add(project));
                uow.Commit();

                Action<List<Project>, List<string>> checker = (retrievedProject, expectedProjecNames) =>
                {
                    Assert.AreEqual(retrievedProject.Count, expectedProjecNames.Count,"Difference between inserted and retrieved");

                    foreach (var project in retrievedProject)
                    {
                        var pName = project.Name;
                        Assert.IsTrue(expectedProjecNames.Contains(pName), $"Project: '{pName}' not found");
                        expectedProjecNames.Remove(pName);
                    }
                    Assert.IsTrue(expectedProjecNames.Count == 0, "All the projects should have been found");
                };

                checker(uow.Projects.GetAll().Result.ToList(), names);

                checker(uow.Projects.GetAllRootProjects().Result.ToList(), new List<string>() {"P1","P2","P3"});

            }
        }

        [Test]
        public void TestFindByUrl()
        {
            #region Test data
            var p1 = TestHelper.GetOneEmptyOneProject();
            var p1A = TestHelper.GetOneEmptyOneProject();
            var p1AA = TestHelper.GetOneEmptyOneProject();

            var p2 = TestHelper.GetOneEmptyOneProject();
            var p1B = TestHelper.GetOneEmptyOneProject();
            var p1BB = TestHelper.GetOneEmptyOneProject();
            

            p1.AddSubProject(p1A);
            p1.AddSubProject(p1B);

            p1A.AddSubProject(p1AA);
            p1A.AddSubProject(p1BB);

            var p1Url = p1.Url;
            var p1BUrl = p1B.Url;
            var p1AAUrl = p1AA.Url;
            var p2Url = p2.Url;
            #endregion

            FileRepoDbContext.Projects.AddRange(new List<Project> { p1, p2 });
            FileRepoDbContext.SaveChanges();

            var dRepo = new ProjectRepository(FileRepoDbContext);

            p1 = dRepo.FindByUrl(p1Url).Result;
            p1B = dRepo.FindByUrl(p1BUrl).Result;
            p1AA = dRepo.FindByUrl(p1AAUrl).Result;
            p2 = dRepo.FindByUrl(p2Url).Result;
            const string errMsg = "Project not found using url";

            Assert.AreEqual(p1Url, p1.Url, errMsg);
            Assert.AreEqual(p1BUrl, p1B.Url, errMsg);
            Assert.AreEqual(p1AAUrl, p1AA.Url, errMsg);
            Assert.AreEqual(p2Url, p2.Url, errMsg);

        }
    }
}
