﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Data.Repository.Base;
using FileRepo.Data;
using FileRepo.Model;

namespace FileRepo.Data.Repository
{
    public class ProjectRepository : FileRepoRepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(FileRepoDbContext context) : base(context)
        {
        }
       
        public QueryResult<Project, Exception> GetBiggestProject()
        {
            Func<Project> call = () => FileRepoDbContext.Projects
                .OrderByDescending(project => project.Id)
                .ThenByDescending(project => project.Documents.Count)
                .FirstOrDefault();

            return CallWrapper(call, "Unable to retrieve the biggest project.");
        }

        public new QueryResult<IEnumerable<Project>, Exception> GetAllRootProjects()
        {
           return base.GetAllRootProjects();
        }

        public QueryResult<Project, Exception> FindByUrl(string url)
        {
            Func<Project> call = () =>
            {
               var splittedUrl = url.Split(new[] {@"\"}, StringSplitOptions.RemoveEmptyEntries);
               var urlLength = splittedUrl.Length;

                var elementName = splittedUrl.Last();
                Project projectFound = null;
                switch (urlLength)
                {
                    case 0://empty url
                        break;
                    case 1://element is at the root of the repository
                       projectFound = GetAllRootProjects().Result.First(project => project.Name == elementName);
                        break;
                    default://element is deep into the repositorys
                        var projectsToSeekIn = FindParentBasedOnUrl(url).Result.SubProjects;
                        projectFound = projectsToSeekIn?.FirstOrDefault(p => p.Name == elementName);
                        break;
                };
                
                return projectFound;
            };

            return CallWrapper(call, $"Unable to find project using this url: '{url}'");
        }
    }
}
