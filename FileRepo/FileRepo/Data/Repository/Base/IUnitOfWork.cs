﻿using System;

namespace FileRepo.Data.Repository.Base
{
  public  interface IUnitOfWork : IDisposable
    {
        IProjectRepository Projects { get; }
        IDocumentRepository Documents { get; }
        QueryResult<int, Exception> Commit();
    }
}
