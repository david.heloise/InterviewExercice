﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Model;

namespace FileRepo.Data.Repository.Base
{
   public interface IFileRepoRepository<TEntity> where TEntity:RepositoryEntitytBase
    {
        /// <summary>
        /// Find a repository element using its url
        /// </summary>
        /// <param name="url">Targeted element's url</param>
        /// <returns>Element</returns>
        QueryResult<TEntity, Exception> FindByUrl(string url);
    }
}
