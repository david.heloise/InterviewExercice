﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Model;

namespace FileRepo.Data.Repository.Base
{
    public interface IProjectRepository : IFileRepoRepository<Project>, IRepository<Project>
    {
        /// <summary>
        /// Get the lastest biggest project in terms of number of document its contain
        /// </summary>
        /// <returns>A project</returns>
        QueryResult<Project, Exception> GetBiggestProject();

        /// <summary>
        /// Get all the projects at the root of the document repository
        /// </summary>
        /// <returns>A collection of projects</returns>
        QueryResult<IEnumerable<Project>, Exception> GetAllRootProjects();

    }
}
