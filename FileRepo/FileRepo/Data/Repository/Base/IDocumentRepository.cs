﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Model;

namespace FileRepo.Data.Repository.Base
{
    public interface IDocumentRepository : IFileRepoRepository<Document>, IRepository<Document>
    {
        /// <summary>
        /// Get the lastest docummment based on its id
        /// </summary>
        /// <returns>a document</returns>
        QueryResult<Document,Exception> GetLastestDocument();
    }
}
