﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace FileRepo.Data.Repository.Base
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;
        
        public Repository(DbContext context)
        {
            Context = context;
        }
            
        #region IRepository implementation
        public virtual QueryResult<TEntity, Exception> Add(TEntity entity)
        {
            Func<TEntity> call = () => Context.Set<TEntity>().Add(entity);
            return CallWrapper(call, "Unable to add.");
        }

        public QueryResult<TEntity, Exception> Remove(TEntity entity)
        {
            Func<TEntity> call = () => Context.Set<TEntity>().Remove(entity);
            return CallWrapper(call, "Unable to remove.");
        }

        public QueryResult<TEntity, Exception> Get(int id)
        {
            Func<TEntity> call = () => Context.Set<TEntity>().Find(id);
            return CallWrapper(call, $"Unable to find using id :{id}");
        }
      
        public QueryResult<IEnumerable<TEntity>, Exception> GetAll()
        {
            Func<IEnumerable<TEntity>> call = () => Context.Set<TEntity>().AsEnumerable();
            return CallWrapper(call , "Unable to get all.");
        }
        
        public QueryResult<IEnumerable<TEntity>, Exception> Find(Expression<Func<TEntity, bool>> predicate)
        {
            Func<IEnumerable<TEntity>> call = ()=> Context.Set<TEntity>().Where(predicate);
            return CallWrapper(call, "Unable to find.");
        }
        #endregion

        protected QueryResult<TOutputType, Exception> CallWrapper<TOutputType>(Func<TOutputType> call, string errorMessage)
        {
            Exception error = null;
            var result = default(TOutputType);
            try
            {
                if (call != null)
                    result = call();
            }
            catch (Exception exception)
            {
                error = exception;
            }

            return error == null
                 ? new QueryResult<TOutputType, Exception>(result)
                 : new QueryResult<TOutputType, Exception>(result, error, errorMessage);
        }
    }
}
