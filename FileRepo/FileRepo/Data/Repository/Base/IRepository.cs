﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FileRepo.Data.Repository.Base
{
    public interface IRepository<TEntity> where TEntity : class
    {
        QueryResult<TEntity, Exception> Add(TEntity entity);
        QueryResult<TEntity, Exception> Remove(TEntity entity);
        QueryResult<TEntity, Exception> Get(int id);
        QueryResult<IEnumerable<TEntity>, Exception> GetAll();
        QueryResult<IEnumerable<TEntity>, Exception> Find(Expression<Func<TEntity, bool>> predicate);
    }
}
