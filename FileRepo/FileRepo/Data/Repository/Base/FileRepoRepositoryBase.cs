﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Data;
using FileRepo.Model;

namespace FileRepo.Data.Repository.Base
{
    public class FileRepoRepositoryBase<TEntity> : Repository<TEntity> where TEntity : RepositoryEntitytBase
    {
        public FileRepoDbContext FileRepoDbContext => Context as FileRepoDbContext;

        public FileRepoRepositoryBase(DbContext context) : base(context)
        {
        }

        protected virtual QueryResult<IEnumerable<Project>, Exception> GetAllRootProjects()
        {
            Func<IEnumerable<Project>> call =
                () => FileRepoDbContext.Projects.Include(p=>p.Documents).Include(p=>p.SubProjects.Select(sp=>sp.Documents)).Where(project => project.Parent == null);

            return CallWrapper(call, "Unable to retrieve root project.");
        }

        /// <summary>
        /// Search the parent project of an element using the element's url
        /// </summary>
        /// <param name="url">Url of the elememt</param>
        /// <returns>Parent project of the targeted element</returns>
        protected QueryResult<Project, Exception> FindParentBasedOnUrl(string url)
        {
            Func<Project> call = () =>
            {
                var projectsToSeekIn = GetAllRootProjects().Result;
                Project foundElement = null;

                var splitUrl = url.Split(new[] { @"\" }, StringSplitOptions.RemoveEmptyEntries);

                for (int index = 0; index < splitUrl.Length - 1; index++)
                {
                    var elementName = splitUrl[index];
                    var isTheParent = index == splitUrl.Length - 2;

                    var matchingProject = projectsToSeekIn?.FirstOrDefault(p => p.Name == elementName);

                    if (matchingProject == null)
                        break;

                    if (!isTheParent)
                        projectsToSeekIn = matchingProject.SubProjects;
                    else
                        foundElement = matchingProject;
                }
                return foundElement;
            };

            return CallWrapper(call,
                "Fail to retrieve the parent project of the targeted element. Make sure the url is correct.");
        }

    }
}
