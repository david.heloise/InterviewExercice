﻿namespace FileRepo.Data.Repository
{
    /// <summary>
    /// Class used to wrap result of queries
    /// </summary>
    /// <typeparam name="TResult">Expected result data type</typeparam>
    /// <typeparam name="TError">Error type (usualy Exception)</typeparam>
    public class QueryResult<TResult,TError>
    {
        public TResult Result { get; private set; }
        public TError Error { get; private set; }
        /// <summary>
        /// True is the query has succeed
        /// </summary>
        public bool HasSucceed { get; private set; }
        public string Message { get; private set; }

        /// <summary>
        /// Use this constructor only for SUCCEED queries
        /// </summary>
        /// <param name="result">Result data</param>
        /// <param name="message">Optional succeed message (default="Succeed")</param>
        public QueryResult(TResult result, string message = "Succeed")
        {
            HasSucceed = true;
            Message = message;
            Result = result;
        }

        /// <summary>
        /// Use this constructor only for FAILED queries
        /// </summary>
        /// <param name="result">Result data</param>
        /// <param name="error">Error data</param>
        /// <param name="message">Optional failure explanation message (default="Failed")</param>
        public QueryResult(TResult result, TError error, string message = "Failed")
        {
            HasSucceed = false;
            Message = message;
            Result = result;
            Error = error;
        }

    }
}
