﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Data.Repository.Base;
using FileRepo.Data;

namespace FileRepo.Data.Repository
{
    public class FileRepoUnitOfWork : IUnitOfWork
    {
        private readonly FileRepoDbContext _context;

        public IProjectRepository Projects { get; }
        public IDocumentRepository Documents { get; }

        public FileRepoUnitOfWork(FileRepoDbContext dbContext)
        {
            _context = dbContext;
            Projects = new ProjectRepository(_context);
            Documents = new DocumentRepository(_context);
        }

        #region IUnitOfWorkImplementation

        public QueryResult<int,Exception> Commit()
        {
            try
            {
                return new QueryResult<int, Exception>(_context.SaveChanges());
            }
            catch (Exception ex)
            {
                return new QueryResult<int, Exception>(-1, ex,"Fail to commit data.");
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        #endregion
    }
}
