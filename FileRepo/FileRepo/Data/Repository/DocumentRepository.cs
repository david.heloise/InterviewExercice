﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Data.Repository.Base;
using FileRepo.Migrations;
using FileRepo.Model;

namespace FileRepo.Data.Repository
{
  public class DocumentRepository : FileRepoRepositoryBase<Document>, IDocumentRepository
  {
      private readonly HashSet<string> _validExtensions;

        public DocumentRepository(DbContext context) : base(context)
        {
            if (Properties.Settings.Default.Extensions != null)
            {
                _validExtensions = new HashSet<string>();
                foreach (var extension in Properties.Settings.Default.Extensions)
                    if (extension != null && !_validExtensions.Contains(extension))
                        _validExtensions.Add(extension);
            }
                
        }
        
        public QueryResult<Document, Exception> GetLastestDocument()
        {
            Func<Document> call = 
                () => FileRepoDbContext.Documents
                .OrderByDescending(document => document.Id)
                .FirstOrDefault();

            return CallWrapper(call, "Unable to retrieve lastest document.");
        }

      public override QueryResult<Document, Exception> Add(Document document)
      {
            Func<Document> call = () =>
            {
                if (document.Parent == null)
                    throw new InvalidOperationException("Document shoul always be attached to a project");

                if (_validExtensions != null && !_validExtensions.Contains(document.Extension))
                    throw new InvalidOperationException("Document shoul always be attached to an extension");

                return base.Add(document).Result;
            };

            return CallWrapper(call, "Unable to add document");
        }

      public QueryResult<Document, Exception> FindByUrl(string url)
      {
          Func<Document> call = () =>
          {
              var elementName = url.Split(new[] {@"\"}, StringSplitOptions.RemoveEmptyEntries).Last();
              Document documentFound = null;
              var parentProject = FindParentBasedOnUrl(url);
              if (parentProject.HasSucceed)
                  documentFound = parentProject.Result.Documents.FirstOrDefault(document => $"{document.Name}.{document.Extension}" == elementName);

              return documentFound;

          };
            return CallWrapper(call, $"Unable to find document using this url: '{url}'");
        }
  }
}
