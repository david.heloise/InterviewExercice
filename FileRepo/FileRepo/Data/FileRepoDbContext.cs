﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Model;

namespace FileRepo.Data
{
   public class FileRepoDbContext : DbContext
    {
       public DbSet<Project> Projects { get; set; }
       public DbSet<Document> Documents { get; set; }

       public FileRepoDbContext()
            :base("default")
        { }

    }
}
