﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileRepo.Model
{
    public class Project : RepositoryEntitytBase
    {
        public IList<Document> Documents { get; set; } = new List<Document>();

        public IList<Project> SubProjects { get; set; } = new List<Project>();
        
    }
}
