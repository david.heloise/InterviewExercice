﻿using System;
using System.Collections.Generic;
using System.IO;
using FileRepo.Data.Repository;
using FileRepo.Data.Repository.Base;

namespace FileRepo.Model
{
    public static class Extentions
    {
        private static readonly int ProjectMaxDisplayDeth = Properties.Settings.Default.ProjectMaxDisplayDepth;
        public const string DefaultDirectory = @".\Downloads";

        private static string AsFormatedText(this Document document)
        {
            return
                $"Document: {document.Name} - Extension: .{document.Extension} - URL: {document.Url}";
        }

        private static string AsFormatedText(this Project project)
        {
            return $"Project: {project.Name} - URL: {project.Url}";
        }

        public static void ShowInConsole(this Project project)
        {
            ShowInConsole(project, "");
        }

        private static void ShowInConsole(Project project, string offSet, int level = 0)
        {
            if (level > ProjectMaxDisplayDeth)
            {

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($@"{offSet}/!\  WARNING /!\  -  This project has too much sub-levels so it cannot be display.");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            if (project == null) return;
            var documents = project.Documents;
            var subprojects = project.SubProjects;
            
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"{offSet}¤ {project.AsFormatedText()}");
            Console.ForegroundColor = ConsoleColor.White;

            offSet += "\t";
            foreach (var document in documents)
                Console.WriteLine($"{offSet}- {document.AsFormatedText()}");

            foreach (var subProject in subprojects)
                ShowInConsole(subProject, offSet += "\t", ++level);
        }

        public static QueryResult<Project, Exception> Download(this Project project, string rootDirectory = DefaultDirectory)
        {
            QueryResult<Project, Exception> result = null;
            try
            {
                var directoryPath = Path.Combine(rootDirectory, project.Name);
                Directory.CreateDirectory(directoryPath);

                foreach (var document in project.Documents)
                    document.Download(directoryPath);

                foreach (var subProject in project.SubProjects)
                    subProject.Download(directoryPath);
            }
            catch (Exception ex)
            {
                result = new QueryResult<Project, Exception>(project,ex,"Fail to download project.");
            }

            return result ?? new QueryResult<Project, Exception>(project);
        }

        public static QueryResult<Document, Exception> Download(this Document docmument, string targetDirectory= DefaultDirectory)
        {
            QueryResult<Document, Exception> result = null;
            try
            {
                var filePath = Path.Combine(targetDirectory, $"{docmument.Name}.{docmument.Extension}");
                File.WriteAllBytes(filePath, docmument.Value);
            }
            catch (Exception ex)
            {
                result = new QueryResult<Document, Exception>(docmument, ex, "Fail to download project.");
            }
            return result ?? new QueryResult<Document, Exception>(docmument);
        }

        public static void AddDocument(this Project project, Document document)
        {
            if (project != null && document != null)
            {
                document.Parent = project;
                project.Documents.Add(document);
            }
        }

        public static void AddSubProject(this Project project, Project childProject)
        {
            if (project != null && childProject != null)
            {
                childProject.Parent = project;
                project.SubProjects.Add(childProject);
            }
        }

        public static void AddDocuments(this Project project, IEnumerable<Document> documents)
        {
            foreach (var document in documents)
                project.AddDocument(document);
        }
    }
}
