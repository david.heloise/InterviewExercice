﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileRepo.Model
{
    public class Document : RepositoryEntitytBase
    {
        [Required]
        public byte[] Value { get; set; } = new byte[1];

        [Required]
        public string Extension { get; set; }

        public new string Url => $"{base.Url}.{Extension}";
    }
}
