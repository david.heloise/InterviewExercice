﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileRepo.Model
{
    public class RepositoryEntitytBase
    {
        public Project Parent { get; set; }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Url
        {
            get
            {
                var parentUrl = Parent != null ? Parent.Url : string.Empty;

                return $@"{parentUrl}\{Name}";
            }
        }
    }
}
