﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileRepo.Data;
using FileRepo.Data.Repository;
using FileRepo.Data.Repository.Base;
using FileRepo.Model;

namespace FileRepo
{
    class Program
    {
        static FileRepoUnitOfWork _fileRepoSystemUnitOfWork;

        static void Main(string[] args)
        {
            using (_fileRepoSystemUnitOfWork = new FileRepoUnitOfWork(new FileRepoDbContext()))
            {
               //InitExerciceData();
                ShowRepositoryContent();
                LoopMenu();
            }
        }

        static void LoopMenu()
        {
            while (true)
            {
                Console.WriteLine("\n____________________________________\n");
                Console.WriteLine("Choose an operation: ");
                Console.WriteLine("\t  F1 => Display repository");
                Console.WriteLine("\t  F2 => Add project");
                Console.WriteLine("\t  F3 => Add document");
                Console.WriteLine("\t  F4 => Dowload document");
                Console.WriteLine("\t  F5 => Dowload project");
                Console.WriteLine("\t  F6 => Delete project");
                Console.WriteLine("\t  F7 => Delete document");
                Console.WriteLine("\t  F8 => Clear console");
                Console.WriteLine("\t Esc => Quit");
                Console.WriteLine("____________________________________\n");
                var key = Console.ReadKey(true).Key;

                switch (key)
                {
                    case ConsoleKey.F1:
                        ShowRepositoryContent();
                        break;
                    case ConsoleKey.F2:
                        CreateProject();
                        break;
                    case ConsoleKey.F3:
                        PushDocument();
                        break;
                    case ConsoleKey.F4:
                        DownloadDocument();
                        break;
                    case ConsoleKey.F5:
                        DownloadProject();
                        break;
                    case ConsoleKey.F6:
                        DeleteProject();
                        break;
                    case ConsoleKey.F7:
                        DeleteDocument();
                        break;
                    case ConsoleKey.F8:
                        Console.Clear();
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        Console.WriteLine("Unknown operation");
                        break;
                }
            }
        }
     

        /// <summary>
        /// Initiate and show data asked for the exercice
        /// </summary>
        static void InitExerciceData()
        {
            var p1Documents = new List<Document>
                {
                    new Document {Name = "WordFile1", Extension = "doc"},
                    new Document {Name = "ExcelFile1", Extension = "xls"}
                };

            var p2Documents = new List<Document>
                {
                    new Document {Name = "WordFile2", Extension = "doc"},
                    new Document {Name = "ExcelFile2", Extension = "xls"}
                };

            var p1SubProject = new Project() { Name = "Project A" };
            var p1 = new Project { Name = "Project 1" };
            var p2 = new Project { Name = "Project 2" };
            var p3 = new Project { Name = "Project 3" };

            var pptDoc = new Document { Name = "PPTFile1", Extension = "ppt" };
            p1SubProject.AddDocument(pptDoc);
            p1.AddDocuments(p1Documents);
            p1.AddSubProject(p1SubProject);
            p2.AddDocuments(p2Documents);

            _fileRepoSystemUnitOfWork.Projects.Add(p1);
            _fileRepoSystemUnitOfWork.Projects.Add(p2);
            _fileRepoSystemUnitOfWork.Projects.Add(p3);
            _fileRepoSystemUnitOfWork.Commit();
        }
        
        /// <summary>
        /// Display repository content in a hierarchical manner
        /// </summary>
        static void ShowRepositoryContent()
        {
            var queryResult = _fileRepoSystemUnitOfWork.Projects.GetAllRootProjects();

            if (queryResult.HasSucceed)
                foreach (var project in queryResult.Result)
                    project.ShowInConsole();
            else
                EndOfOperation(queryResult.Message);
        }
        
        #region Project related
        /// <summary>
        /// Engage UI interaction to add a new project
        /// </summary>
        static Project CreateProject()
        {
            Console.WriteLine("Enter new project name: ");

            var projectName = Console.ReadLine();
            var newProject = new Project { Name = projectName };
            if (IsYes("Do you want to add this project into an existing one ?"))
            {
              var  parentProject = GetByUrl<Project>();
                if (parentProject == null)
                    return null;

                newProject.Parent = parentProject;
            }

            var addResult = _fileRepoSystemUnitOfWork.Projects.Add(newProject);

            if (!addResult.HasSucceed)
            {
                EndOfOperation(addResult.Message);
                return null;
            }

            var commitResult = _fileRepoSystemUnitOfWork.Commit();
            if (!commitResult.HasSucceed)
            {
                EndOfOperation(commitResult.Message);
                return null;
            }

            EndOfOperation(commitResult.Message, commitResult.HasSucceed);
            return newProject;

        }
        #endregion
        
        #region Document related

        private static void PushDocument()
        {
            Console.WriteLine("Enter the fullpath of the file you want to push:");
            var filePath = Console.ReadLine();
            var fileMaxSize = Properties.Settings.Default.MaxFileSizeInOctet;
            byte[] fileData = null;

            #region Check & Get file 

            if (!File.Exists(filePath))
            {
                EndOfOperation("The given file in not accessible.");
                return;
            }

            var documentExtension = Path.GetExtension(filePath).Replace(".", "");
            if (!Properties.Settings.Default.Extensions.Contains(documentExtension))
            {
                EndOfOperation("The given file type is not allow in this repository.");
                return;
            }

            try
            {
                fileData = File.ReadAllBytes(filePath);

                if (fileData.Length > fileMaxSize)
                {
                    EndOfOperation($"File should not be bigger than {fileMaxSize/1024}Ko");
                    return;
                }
            }
            catch (Exception)
            {
                EndOfOperation("Unable to read file.");
                return;
            }

            #endregion

            #region Parent project determination

            Project parentProject = null;

            if (IsYes("Do you want to put it in a new project ?"))
            {
                parentProject = CreateProject();
                if (parentProject == null)
                {
                    EndOfOperation("Unable to add new document.");
                    return;
                }
            }
            else
            {

                parentProject = GetByUrl<Project>();

                if (parentProject == null)
                {
                    EndOfOperation("Fail to find project - Unable to add new document.");
                    return;
                }
            }

            #endregion

            var documentName = Path.GetFileNameWithoutExtension(filePath);

            if (IsYes("Do you want to change the name of the document ?"))
            {
                Console.WriteLine("Enter document name:");
                documentName = Console.ReadLine();
            }

            var newDocument = new Document
            {
                Name = documentName,
                Extension = documentExtension,
                Value = fileData,
                Parent = parentProject
            };
            var addResult = _fileRepoSystemUnitOfWork.Documents.Add(newDocument);

            if (!addResult.HasSucceed)
            {
                EndOfOperation(addResult.Message);
                return;
            }

            var commitResult = _fileRepoSystemUnitOfWork.Commit();
            EndOfOperation(commitResult.Message, commitResult.HasSucceed);
        }

        /// <summary>
        /// Delete a document via its url
        /// </summary>
        private static void DeleteDocument()
        {
            DeleteOrDowloadByUrl<Document>(_fileRepoSystemUnitOfWork.Documents.Remove);
        }

        /// <summary>
        /// Download a document find via its url.
        /// The document will be store in the working directory
        /// </summary>
        private static void DownloadDocument()
        {
            DeleteOrDowloadByUrl<Document>(document => {
                var result = document.Download();
                if (result.HasSucceed) Process.Start(Extentions.DefaultDirectory);
                return result;
            });
        }

        /// <summary>
        /// Delete a project via its url
        /// </summary>
        private static void DeleteProject()
        {
            DeleteOrDowloadByUrl<Project>(_fileRepoSystemUnitOfWork.Projects.Remove);
        }

        /// <summary>
        /// Download a project find via its url.
        /// The document will be store in the working directory
        /// </summary>
        private static void DownloadProject()
        {
            DeleteOrDowloadByUrl<Project>(project =>{
                var result = project.Download();
                if (result.HasSucceed) Process.Start(Extentions.DefaultDirectory);
                return result;
            });
        }

        private static void DeleteOrDowloadByUrl<TEntity>(Func<TEntity,QueryResult<TEntity,Exception>> func) where TEntity : RepositoryEntitytBase
        {
            var toWorkOn = GetByUrl<TEntity>();

            if (toWorkOn == null)
                return;

            var result = func(toWorkOn);
            if (!result.HasSucceed)
            {
                EndOfOperation(result.Message);
                return;
            }

            var commitResult = _fileRepoSystemUnitOfWork.Commit();
            EndOfOperation(commitResult.Message, commitResult.HasSucceed);
        }

        private static TEntity GetByUrl<TEntity>() where TEntity : RepositoryEntitytBase
        {
            RepositoryEntitytBase result = null;
            var isDocument = typeof(TEntity) == typeof(Document);

            Console.WriteLine("Enter url: ");
            var url = Console.ReadLine();

            QueryResult<TEntity, Exception> findResult;
            if (isDocument)
                findResult = _fileRepoSystemUnitOfWork.Documents.FindByUrl(url) as QueryResult<TEntity, Exception>;
            else
                findResult = _fileRepoSystemUnitOfWork.Projects.FindByUrl(url) as QueryResult<TEntity, Exception>;

            if (findResult == null || !findResult.HasSucceed)
                EndOfOperation(findResult?.Message);
            else
                result = findResult.Result;

            return (TEntity)result;
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Display the result of an interaction
        /// </summary>
        /// <param name="message"></param>
        /// <param name="hasSucceed"></param>
        private static void EndOfOperation(string message, bool hasSucceed = false)
        {
            if (!hasSucceed)
                Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Ask a Yes/No question to the user until he answer properly
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        static bool IsYes(string question)
        {
            var input = "noSet";
            var validInput = new[] { "Y", "N", "" };

            while (!validInput.Contains(input))
            {
                Console.WriteLine($"\n{question} (y/N) ");
                input = Console.ReadLine()?.ToUpper();
            }

            return input.ToUpper() == "Y";
        } 
        #endregion

    }
}
