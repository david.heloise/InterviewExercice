namespace FileRepo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitFileRepoModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Binary(nullable: false),
                        Extension = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Parent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Parent_Id)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Parent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Parent_Id)
                .Index(t => t.Parent_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "Parent_Id", "dbo.Projects");
            DropForeignKey("dbo.Documents", "Parent_Id", "dbo.Projects");
            DropIndex("dbo.Projects", new[] { "Parent_Id" });
            DropIndex("dbo.Documents", new[] { "Parent_Id" });
            DropTable("dbo.Projects");
            DropTable("dbo.Documents");
        }
    }
}
